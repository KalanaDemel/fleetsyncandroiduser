package io.codly.Fleesync1.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.app.Activity;
import io.codly.Fleesync1.R;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;


public class Profile extends Activity {
	
	
	public static String pNo="";
	
		
	
	
	String pass, email, fname, lname;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_profile);
		
		Bundle extras = getIntent().getExtras();
	 	if (extras != null) {
	 	    pNo=extras.getString("pno");
	 	}
		
		List<NameValuePair> params;
		params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("pno", pNo));
		
		ServerRequest sr = new ServerRequest();
        JSONObject json = sr.getJSON("http://nodejs-fleetsyncanrd.rhcloud.com/getProfile",params);
        
        if(json != null){
            try{
            String jsonstr = json.getString("response");
                if(json.getBoolean("res")){
                	
                    String phone=json.getString("phone");
                    String password=json.getString("password");
                    String email=json.getString("email");
                    String fname=json.getString("fname");
                    String lname=json.getString("lname");
                    String enableRS=json.getString("enableRS");
                    String ownsV=json.getString("ownsV");
                    String gender=json.getString("gender");
                    
                    EditText phoneno = (EditText)findViewById(R.id.phone);
                    phoneno.setText(phone);
                    EditText pword = (EditText)findViewById(R.id.pass);
                    pword.setText(password);
                    EditText emal = (EditText)findViewById(R.id.emal);
                    emal.setText(email);
                    EditText firstname = (EditText)findViewById(R.id.first);
                    firstname.setText(fname);
                    EditText lastname = (EditText)findViewById(R.id.last);
                    lastname.setText(lname);
                    
                    Switch gnder = (Switch)findViewById(R.id.gender);
                    Switch enableRdeSharing = (Switch)findViewById(R.id.enableRideSharing);
                    Switch ownsVehicle = (Switch)findViewById(R.id.ownsVehicle);
                    
                    if(gender.equals("Male")) {                    	
                    	gnder.setChecked(false);            		 	
            	    }                     
                    if(enableRS.equals("Yes")) {                    	
                    	enableRdeSharing.setChecked(false);            		 	
            	    } 
                    if(ownsV.equals("Yes")) {                    	
                    	ownsVehicle.setChecked(false);            		 	
            	    }
                    
               	 
                }
                else{
               	 Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
                }

            }catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "Error Invalid login", Toast.LENGTH_SHORT).show();
            }
            }
        
        
		initView();
		ImageView img = (ImageView)findViewById(R.id.imageView1);
        img.setBackgroundResource(R.drawable.myprogress);

        // Get the background, which has been compiled to an AnimationDrawable object.
        AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();

        // Start the animation (looped playback by default).
        frameAnimation.start();
	}

    private void initView(){
        

        //TODO add extra view initialization code here
    }

    
    public void onAddphotoClicked(View v){
        Intent intent = new Intent(this, Upimgprofile.class);
        startActivity(intent);
    }

    public void onBackClicked(View v){
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

    public void onConfirmClicked(View v){
    	
    	final EditText phoneno = (EditText)findViewById(R.id.phone);        
        final EditText pword = (EditText)findViewById(R.id.pass);        
        final EditText emal = (EditText)findViewById(R.id.emal);        
        final EditText firstname = (EditText)findViewById(R.id.first);        
        final EditText lastname = (EditText)findViewById(R.id.last);
        
        final List<NameValuePair> params;
        params = new ArrayList<NameValuePair>();
        
        pass= pword.getText().toString();
        email= emal.getText().toString();
        fname= firstname.getText().toString();
        lname= lastname.getText().toString();
        
        
        //Checking Text changes
        pword.addTextChangedListener(new TextWatcher() {

     	   @Override
     	   public void afterTextChanged(Editable s) {     		   
     		   pass=pword.toString();
     	   }
     	   @Override    
     	   public void beforeTextChanged(CharSequence s, int start,
     	     int count, int after) {
     	   }
     	   @Override    
     	   public void onTextChanged(CharSequence s, int start,
     	     int before, int count) {
     	     
     	   }
     	  });
        emal.addTextChangedListener(new TextWatcher() {

     	   @Override
     	   public void afterTextChanged(Editable s) {
     		 email=emal.toString();     		   
     	   }
     	   @Override    
     	   public void beforeTextChanged(CharSequence s, int start,
     	     int count, int after) {
     	   }
     	   @Override    
     	   public void onTextChanged(CharSequence s, int start,
     	     int before, int count) {     	     
     	   }
     	  });
        firstname.addTextChangedListener(new TextWatcher() {

     	   @Override
     	   public void afterTextChanged(Editable s) {
     		 fname=firstname.toString();     		   
     	   }
     	   @Override    
     	   public void beforeTextChanged(CharSequence s, int start,
     	     int count, int after) {
     	   }
     	   @Override    
     	   public void onTextChanged(CharSequence s, int start,
     	     int before, int count) {     	     
     	   }
     	  });
        lastname.addTextChangedListener(new TextWatcher() {

     	   @Override
     	   public void afterTextChanged(Editable s) {
     		  lname=lastname.toString();
     	   }
     	   @Override    
     	   public void beforeTextChanged(CharSequence s, int start,
     	     int count, int after) {
     	   }
     	   @Override    
     	   public void onTextChanged(CharSequence s, int start,
     	     int before, int count) {     	     
     	   }
     	  });   	       
        
        
        //Switch gender=(Switch)findViewById(R.id.gender);
		//Switch ownsVehicle=(Switch)findViewById(R.id.ownsVehicle);
        //Switch EnableRideShare=(Switch)findViewById(R.id.enableRideSharing);
        
        String gder, enableRS, OwnnsV;
        
        Switch gnder = (Switch)findViewById(R.id.gender);
        Switch enableRdeSharing = (Switch)findViewById(R.id.enableRideSharing);
        Switch ownsVehicle = (Switch)findViewById(R.id.ownsVehicle);
    	
        if(gnder.isChecked()) {
		 	gder="Female";
	    } else {
	    	gder="Male";
	    }
        
        if(enableRdeSharing.isChecked()) {
		 	enableRS="No";
	    } else {
	    	enableRS="Yes";
	    }
        if(ownsVehicle.isChecked()) {
		 	OwnnsV="No";
	    } else {
	    	OwnnsV="Yes";
	    }
    	
        //Querying DB for update
        params.add(new BasicNameValuePair("pno", pNo));
		params.add(new BasicNameValuePair("password", pass)); 
		params.add(new BasicNameValuePair("email", email)); 
		params.add(new BasicNameValuePair("firstname", fname)); 
  		params.add(new BasicNameValuePair("lastname", lname));
  		params.add(new BasicNameValuePair("enableRS", enableRS));
        params.add(new BasicNameValuePair("OwnsVehicle", OwnnsV));
        params.add(new BasicNameValuePair("gender", gder));
  		
  		ServerRequest sr = new ServerRequest();
        JSONObject json = sr.getJSON("http://nodejs-fleetsyncanrd.rhcloud.com/updateProfile",params);
        
        if(json != null){
        try
        {
        String jsonstr = json.getString("response");
        
            if(json.getBoolean("res")){            	
             Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();   
           	 
            }
            else{
           	 Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
            }
            

        }
        catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, "Database Error", Toast.LENGTH_SHORT).show();
        }
        }
        
        else{
        	Toast.makeText(this, "Network Error", Toast.LENGTH_SHORT).show();
        	
        }      
        
    	
        /*Intent intent = new Intent(this, Home.class);
        startActivity(intent);*/
    }

    public void onWhatisrsClicked(View v){
        Intent intent = new Intent(this, Whatisrsprofile.class);
        startActivity(intent);
    }


}
