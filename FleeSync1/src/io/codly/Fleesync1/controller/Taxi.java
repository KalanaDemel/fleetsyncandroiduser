package io.codly.Fleesync1.controller;

import java.util.ArrayList;

public class Taxi {
	private String Taxi, passengers, thumbnailUrl, distance, time, cost;
	
	public Taxi(String name, String dis, String passng, String imgurl, String Time, String Cost) {
		this.Taxi = name;
		this.thumbnailUrl = imgurl;
		this.distance = dis;
		this.passengers = passng;	
		this.time=Time;
		this.cost=Cost;
	}
	
	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getTaxi() {
		return Taxi;
	}

	public void setTaxi(String name) {
		this.Taxi = name;
	}
	
	public String getTime() {
		return time;
	}

	public void setTime(String Time) {
		this.time = Time;
	}

	
	public String getDistance() {
		return distance;
	}

	public void setDistance(String dis) {
		this.distance = dis;
	}
	
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getPassenger() {
		return passengers;
	}

	public void setPassenger(String passn) {
		this.passengers = passn;
	}

	

}
