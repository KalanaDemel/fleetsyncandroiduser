package io.codly.Fleesync1.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.content.Intent;
import android.app.Activity;
import io.codly.Fleesync1.R;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;


public class Register extends Activity {
	
	EditText password,firstname,lastname,email;
    Switch gender,ownsVehicle,EnableRideShare;
    String pass, fname, lname, emil;
    String gnder="Female";
    String OwnnsV="No";
    String enableRS="No";
    
    

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
        
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_register);
		
		gender=(Switch)findViewById(R.id.gender);
		gender.setChecked(true);
        ownsVehicle=(Switch)findViewById(R.id.ownsVehicle);
        ownsVehicle.setChecked(true);
        EnableRideShare=(Switch)findViewById(R.id.enableRideSharing);
        EnableRideShare.setChecked(true);
		initView();
	}

    private void initView(){        

        //TODO add extra view initialization code here
    }

    
    public void onAddphotoClicked(View v){
        Intent intent = new Intent(this, Uploadimg.class);
        startActivity(intent);
    }

    public void onBackClicked(View v){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    public void onConfirmClicked(View v){
        
        
        
        
        Bundle extras = getIntent().getExtras();
        String pNo="0"+extras.getString("pno");
        
        password=(EditText)findViewById(R.id.pass);
        firstname=(EditText)findViewById(R.id.first);
        lastname=(EditText)findViewById(R.id.last);
        email=(EditText)findViewById(R.id.emal);
        
                     
              
        if(EnableRideShare.isChecked()) {
		 	enableRS="No";
	    } else {
	    	enableRS="Yes";
	    }
        if(ownsVehicle.isChecked()) {
		 	OwnnsV="No";
	    } else {
	    	OwnnsV="Yes";
	    }
        if(gender.isChecked()) {
		 	gnder="Female";
	    } else {
	    	gnder="Male";
	    }
        
              
     
        pass= password.getText().toString();
        fname= firstname.getText().toString();
        lname= lastname.getText().toString();
        emil= email.getText().toString();
        
        List<NameValuePair> params;
        params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("pno", pNo));        
        params.add(new BasicNameValuePair("password", pass));        
        params.add(new BasicNameValuePair("firstname", fname));
        params.add(new BasicNameValuePair("lastname", lname));
        params.add(new BasicNameValuePair("email", emil));                
        params.add(new BasicNameValuePair("enableRS", enableRS));
        params.add(new BasicNameValuePair("OwnsVehicle", OwnnsV));
        params.add(new BasicNameValuePair("gender", gnder));
       
        
        ServerRequest sr = new ServerRequest();
        JSONObject json = sr.getJSON("http://10.0.3.2:8080/register",params);
        
        if(json != null){
        try
        {
        String jsonstr = json.getString("response");
        
            if(json.getBoolean("res")){            	
             Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();   
           	 Intent intent = new Intent(this, Home.class);
             startActivity(intent);
            }
            else{
           	 Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
            }
            

        }
        catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, "Network Error", Toast.LENGTH_SHORT).show();
        }
        }
        
        else{
        	Toast.makeText(this, "Network Error", Toast.LENGTH_SHORT).show();
        	
        }      
                
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    public void onWhatisrsClicked(View v){
        Intent intent = new Intent(this, Aboutridesharing.class);
        startActivity(intent);
    }
    
   


}
