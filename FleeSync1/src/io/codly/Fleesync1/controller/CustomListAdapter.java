package io.codly.Fleesync1.controller;



import io.codly.Fleesync1.R;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class CustomListAdapter extends BaseAdapter {
	private Activity activity;
	private LayoutInflater inflater;
	private List<Taxi> TaxiItems;
	ImageLoader imageLoader = AppController.getInstance().getImageLoader();

	public CustomListAdapter(Activity activity, List<Taxi> TaxiItems) {
		this.activity = activity;
		this.TaxiItems = TaxiItems;
	}

	@Override
	public int getCount() {
		return TaxiItems.size();
	}

	@Override
	public Object getItem(int location) {
		return TaxiItems.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (inflater == null)
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
			convertView = inflater.inflate(R.layout.list_row, null);

		if (imageLoader == null)
			imageLoader = AppController.getInstance().getImageLoader();
		NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.thumbnail);
		TextView taxi = (TextView) convertView.findViewById(R.id.title);
		TextView passenges = (TextView) convertView.findViewById(R.id.rating);
		TextView time = (TextView) convertView.findViewById(R.id.genre);
		TextView cost = (TextView) convertView.findViewById(R.id.Cost);
		TextView distance = (TextView) convertView.findViewById(R.id.releaseYear);

		// getting Taxi data for the row
		Taxi m = TaxiItems.get(position);

		// thumbnail image
		thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);
		
		// taxi
		taxi.setText(m.getTaxi());
		
		// rating
		passenges.setText("Passengers: " + String.valueOf(m.getPassenger()));
		
		time.setText(m.getTime());
		
		// Cost
		cost.setText(m.getCost());
		
		// release year
		distance.setText(String.valueOf(m.getDistance()));
		

		return convertView;
	}

}