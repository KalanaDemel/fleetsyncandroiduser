package io.codly.Fleesync1.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.StrictMode;
import android.content.Intent;
import android.app.Activity;
import io.codly.Fleesync1.R;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

//twilio API

 


public class Confirmation extends Activity implements OnClickListener{    
    
	
	public static final String ACCOUNT_SID = "ACdaba257416bed2c18ee6d11cbed88aed";
	public static final String AUTH_TOKEN = "3fa650507ec959671e0fcde48b7e5127";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_confirmation);
		initView();
		
		Button button = (Button)findViewById(R.id.buttonregiterphone);
        button.setOnClickListener(this);
	}

    private void initView(){
        

        //TODO add extra view initialization code here
    }

    
    
    /*public void onConfirmClicked(View v) { 	
    	
    	Intent intent = new Intent(this, Confirmverification.class);
        startActivity(intent);    
        
        
    }*/
    

    public void onBackClicked(View v){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);        
        
    }

	@Override
	public void onClick(View v) {	
		
		
		//Getting Phone no from form
		
		EditText pNo = (EditText)findViewById(R.id.phoneno);	
		List<NameValuePair> params;
		String pNumber = pNo.getText().toString();
		
		//Validating pno size
		if (pNumber.matches("")| pNumber.length()!=10 ) {
		    Toast.makeText(this, "You did not enter a valid phone number", Toast.LENGTH_SHORT).show();
		    return;		    
        }
		
		params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("pno", pNumber));
		
		ServerRequest sr = new ServerRequest();
        JSONObject json = sr.getJSON("http://10.0.3.2:8080/chkUser",params);       
        
        
        //check phone number exists in db
        if(json != null){
            try{
            	String jsonstr = json.getString("response");
                if(json.getBoolean("res")){  
                	Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();               	 
                } 
                else{
                	//Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();    
                	Random r = new Random();
        			int code = r.nextInt(99999 - 11111)+11111;
        			String convCode=String.valueOf(code);
        			pNumber=pNumber.substring(1);
        			//Done to avoid Network Exception due to running network operation on main thread, no issue since data transfer is small and short lived. 
        			if (android.os.Build.VERSION.SDK_INT > 9) {
        			    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        			    StrictMode.setThreadPolicy(policy);
        			}
        			HttpClient httpclient = new DefaultHttpClient();

        	    	HttpPost httppost = new HttpPost("https://api.twilio.com/2010-04-01/Accounts/ACdaba257416bed2c18ee6d11cbed88aed/SMS/Messages");
        	    	String base64EncodedCredentials = "Basic "+ Base64.encodeToString((ACCOUNT_SID + ":" + AUTH_TOKEN).getBytes(),Base64.NO_WRAP);

        	    	httppost.setHeader("Authorization",base64EncodedCredentials);
        	    	
        	    	try {

        	    	     List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        	    	     nameValuePairs.add(new BasicNameValuePair("From","+18449121677"));
        	    	     nameValuePairs.add(new BasicNameValuePair("To","+94"+pNumber));
        	    	     nameValuePairs.add(new BasicNameValuePair("Body",convCode));

        	    	     httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        	    	     // Execute HTTP Post Request
        	    	     HttpResponse response = httpclient.execute(httppost);
        	    	     HttpEntity entity = response.getEntity();
        	    	     //System.out.println("Entity post is: "+ EntityUtils.toString(entity));
        	    	     
        	    	    Toast.makeText(this, "Confirm code is sent", Toast.LENGTH_SHORT).show();		
        	 	    	Intent intent = new Intent(this, Confirmverification.class);
        	 	    	intent.putExtra("code",convCode);
        	 	    	intent.putExtra("pno", pNumber);
        	 	        startActivity(intent);
        	    	                    } 
        	    	catch (ClientProtocolException e) {

        	    	                    } 
        	    	catch (IOException e) {

        	    	                    }
                	
                }
               
            }catch (JSONException e) {
                
            }
            }
        
        else{
        	Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();	
        }
       
						
		
	}
    
    

	
}
  
    
    
    



