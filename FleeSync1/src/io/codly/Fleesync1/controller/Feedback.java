package io.codly.Fleesync1.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.app.Activity;
import io.codly.Fleesync1.R;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;



public class Feedback extends Activity {
	
	public String pNo="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_feedback);
		
		Bundle extras = getIntent().getExtras();
	 	if (extras != null) {
	 	    pNo=extras.getString("pno");
	 	}
	 	
	 	List<NameValuePair> params;
		params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("pNo", pNo));
		
		ServerRequest sr = new ServerRequest();
        JSONObject json = sr.getJSON("http://nodejs-fleetsyncanrd.rhcloud.com/getOrder",params);
        
        if(json != null){
            try{
            //String jsonstr = json.getString("response");
                if(json.getBoolean("res")){
                	//Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
                	String jsonstr = json.getString("response");
                	String[] items = new String[]{jsonstr,""};
                	Spinner dropdown2 = (Spinner)findViewById(R.id.spinnerOrders);
                	ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
                	dropdown2.setAdapter(adapter2);
                }
                else{
               	 //Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
                }
                

            }catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "Invalid response", Toast.LENGTH_SHORT).show();
            }
            }
	 	
	 	
	 	
		
	}

    private void initView(){
        

        //TODO add extra view initialization code here
    }

    
    public void onBackClicked(View v){
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

    public void onConfirmClicked(View v){
        Intent intent = new Intent(this, Home.class);      
       	intent.putExtra("pno",pNo);	 	    
	 	
        startActivity(intent);
    }


}
