package io.codly.Fleesync1.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import android.os.Bundle;
import android.os.Handler;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import io.codly.Fleesync1.R;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;


public class Taxilist extends Activity implements Serializable {
	
	// Log tag
	private static final String TAG = Taxilist.class.getSimpleName();

	// Movies json url
	private static final String url = "http://nodejs-fleetsyncanrd.rhcloud.com/taxiList";
	private ProgressDialog pDialog;
	private List<Taxi> taxiList = new ArrayList<Taxi>();
	private ListView listView;
	private CustomListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		listView = (ListView) findViewById(R.id.list);
		adapter = new CustomListAdapter(this, taxiList);
		listView.setAdapter(adapter);
		
		pDialog = new ProgressDialog(this);
		// Showing progress dialog before making http request
		pDialog.setMessage("Loading...");
		pDialog.show();
		
		
        

		// Creating volley request obj
		JsonArrayRequest taxiReq=null;
		taxiReq = new JsonArrayRequest(url,
				new Response.Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray response) {
						Log.d(TAG, response.toString());
						hidePDialog();

						// Parsing json
						
						for (int i = 0; i < response.length(); i++) {
							try {

								JSONObject obj = response.getJSONObject(i);
								Taxi taxi = new Taxi(obj.getString("Taxi"),obj.getString("Distance"),obj.getString("Customers"),obj.getString("imgurl"),obj.getString("time"),obj.getString("cost"));
								
								// adding movie to movies array
								
								taxiList.add(taxi);

							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
						String sortby = null;
						Bundle extras = getIntent().getExtras();
					 	if (extras != null) {
					 		sortby=extras.getString("Sortby");
					 	}
					 	
					 	
					 	if(sortby.equals("Distance")){					 		
					 		Collections.sort(taxiList, new Comparator<Taxi>() {
					 	        @Override
					 	        public int compare(Taxi s1, Taxi s2) {
					 	            return s1.getDistance().compareToIgnoreCase(s2.getDistance());
					 	        }
					 	    });
					 	}
					 	else if(sortby.equals("Time")){					 		
					 		Collections.sort(taxiList, new Comparator<Taxi>() {
					 	        @Override
					 	        public int compare(Taxi s1, Taxi s2) {
					 	            return s1.getTime().compareToIgnoreCase(s2.getTime());
					 	        }
					 	    });
					 	}
					 	else if(sortby.equals("Cost")){
					 		Collections.sort(taxiList, new Comparator<Taxi>() {
					 	        @Override
					 	        public int compare(Taxi s1, Taxi s2) {
					 	            return s1.getCost().compareToIgnoreCase(s2.getCost());
					 	        }
					 	    });
					 	}
					
					 	

						// notifying list adapter about data changes
						// so that it renders the list view with updated data
						adapter.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(TAG, "Error: " + error.getMessage());
						hidePDialog();

					}
				});
		
		// Adding request to request queue		
		AppController.getInstance().addToRequestQueue(taxiReq);
		
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

		      @Override
		      public void onItemClick(AdapterView<?> parent, final View view,
		          int position, long id) {
		        //final String item = (String) parent.getItemAtPosition(position);
		        view.animate().setDuration(500).alpha((float) 0.5)
		            .withEndAction(new Runnable() {
		              @Override
		              public void run() {
		            	  AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(Taxilist.this);
		            	  dlgAlert.setMessage("Are you sure");
		            	  dlgAlert.setTitle("Confirm Order");
		            	  dlgAlert.setPositiveButton("OK",  new DialogInterface.OnClickListener() {
		            	        public void onClick(DialogInterface dialog, int which) {
		            	            //Call Web Service  
		            	        	
		            	        	List<NameValuePair> params;
		            	        	params = new ArrayList<NameValuePair>();
		            	        	String orderID="INV0001";
		            	        	String taxiID="Taxi0001";
		            	        	
		            	        	params.add(new BasicNameValuePair("orderID", orderID));
		            	        	params.add(new BasicNameValuePair("taxiID", taxiID));
		            	        	
		            	        	ServerRequest sr = new ServerRequest();
		            	            //JSONObject json = sr.getJSON("http://10.0.3.2:8080/order",params);
		            	            
		            	            JSONObject json = sr.getJSON("http://fleetride-ridesharefleet.rhcloud.com/acceptReq",params);	            	            
		            	            
		            	            if(json != null){
		            	            try{
		            	            String jsonstr = json.getString("response");
		            	                if(json.getBoolean("res")){
		            	                	//DB operation	            	                        	            
				            	            Bundle extras = getIntent().getExtras();
				            	            String destlat, destlon, loclat, loclon;
											final String pNo;
				            	            
				            	    	 	pNo=extras.getString("pNo");	
				            	    	 	destlat=extras.getString("DestinationLat");	 	
				            	    	 	destlon=extras.getString("DestinationLon");	
				            	    	 	loclat=extras.getString("LocationLat");	
				            	    	 	loclon=extras.getString("LocationLot");	
				            	    	 	
				            	            
				            	            List<NameValuePair> params2 = new ArrayList<NameValuePair>();
				            	            params2.add(new BasicNameValuePair("pno", pNo));
				            	            params2.add(new BasicNameValuePair("orderID", orderID));
				            	        	params2.add(new BasicNameValuePair("taxiID", taxiID));
				            	        	params2.add(new BasicNameValuePair("DestinationLat", destlat));
				            	        	params2.add(new BasicNameValuePair("DestinationLong", destlon));
				            	        	params2.add(new BasicNameValuePair("LocationLat", loclat));
				            	        	params2.add(new BasicNameValuePair("LocationLong", loclon));
				            	        	
				            	        	JSONObject jsondb = sr.getJSON("http://nodejs-fleetsyncanrd.rhcloud.com/order",params2);	
				            	            
				            	            
				            	            if(jsondb != null){
				            	            try{
				            	            String jsonstr2 = json.getString("response");
				            	                if(json.getBoolean("res")){
				            	                	Toast.makeText(getApplication(),jsonstr2,Toast.LENGTH_LONG).show();
				            	                	final Handler handler = new Handler();
				            	                	handler.postDelayed(new Runnable() {
				            	                	  @Override
				            	                	  public void run() {
				            	                		  Intent intent = new Intent(Taxilist.this, Home.class);
				            	                	      startActivity(intent);
				            	                	  }
				            	                	}, 500);
				            	                	
				            	                }
				            	                else{
				            	               	 Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
				            	                }
				            	                

				            	            }catch (JSONException e) {
				            	                e.printStackTrace();
				            	                Toast.makeText(Taxilist.this, "Invalid response", Toast.LENGTH_SHORT).show();
				            	            }
				            	            }
		            	                	Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
		            	                	final Handler handler = new Handler();
		            	                	handler.postDelayed(new Runnable() {
		            	                	  @Override
		            	                	  public void run() {
		            	                		  Intent intent = new Intent(Taxilist.this, Home.class);
		            	                		  intent.putExtra("pno", pNo);
		            	                	      startActivity(intent);
		            	                	  }
		            	                	}, 500);
		            	                	
		            	                }
		            	                else{
		            	               	 Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
		            	                }
		            	                

		            	            }catch (JSONException e) {
		            	                e.printStackTrace();
		            	                Toast.makeText(Taxilist.this, "Invalid response", Toast.LENGTH_SHORT).show();
		            	            }
		            	            }
		            	            
		            	            
		            	          }
		            	      });
		            	  dlgAlert.setNegativeButton("Cancel",  new DialogInterface.OnClickListener() {
		            	        public void onClick(DialogInterface dialog, int which) {
		            	        	view.setAlpha(1);
		            	          }
		            	      });
		            	  dlgAlert.setCancelable(true);
		            	  dlgAlert.create().show();
		              }
		            });
		      }

		    });	
		
		//initView();
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
		hidePDialog();
	}

	private void hidePDialog() {
		if (pDialog != null) {
			pDialog.dismiss();
			pDialog = null;
		}
	}

	

    private void initView(){
        

        //TODO add extra view initialization code here
    }

    
    public void onBackClicked(View v){
        Intent intent = new Intent(this, Order.class);
        startActivity(intent);
    }

    public void onConfirmClicked(View v){
    	
        //Intent intent = new Intent(this, Home.class);
        //startActivity(intent);
    }


}
