package io.codly.Fleesync1.controller;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.app.Activity;
import io.codly.Fleesync1.R;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.TextClock;

public class Home extends Activity {
	
	
	
	private WebView mWebView;
	double latitude;
    double longitude;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_home);		
		
		
		ImageView img = (ImageView)findViewById(R.id.background);
        img.setBackgroundResource(R.drawable.myprogress);
        
        //Getting Location of the User 
	    	  
	    
	    LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
	    //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
	    Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
	    
	    
	    //latitude = location.getLatitude();
		//longitude = location.getLongitude();
	    
	    if (location != null){
	    	latitude = location.getLatitude();
		 	longitude = location.getLongitude();
	    }
	    else{
	    	latitude=79.8428;
		    longitude=6.9344;	
	    }
		
	   
	    //WebView to initiate leaflet html in Assets
        mWebView= (WebView)findViewById(R.id.webView);
        mWebView.clearCache(true);
        mWebView.setWebChromeClient(new WebChromeClient());
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.loadUrl("file:///android_asset/www/map.html?lon="+String.valueOf(latitude)+"&lan="+String.valueOf(longitude)+"");
        mWebView.bringToFront();
        
        // Get the background, which has been compiled to an AnimationDrawable object.
        AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();

        // Start the animation (looped playback by default).
        frameAnimation.start();
	}
	
    private void initView(){
        

        //TODO add extra view initialization code here
    }

    
    public void onProfileClicked(View v){
        Intent intent = new Intent(this, Profile.class);
        Bundle extras = getIntent().getExtras();
        
	 	if (extras != null) {
	 		intent.putExtra("pno", extras.getString("pno"));	 	    
	 	}
	 	//Toast.makeText(this, extras.getString("pno"), Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }

    public void onOrderClicked(View v){
        Intent intent = new Intent(this, Order.class);  
        Bundle extras = getIntent().getExtras();
        
	 	if (extras != null) {
	 		intent.putExtra("pno", extras.getString("pno"));	 	    
	 	}
        startActivity(intent);
    }

    public void onFeedbackClicked(View v){
        Intent intent = new Intent(this, Feedback.class);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
	 		intent.putExtra("pno", extras.getString("pno"));	 	    
	 	}
        startActivity(intent);
    }
    public void onGamesClicked(View v){
    	 
    	
    	

    }



}
