package io.codly.Fleesync1.controller;

import android.os.Bundle;
import android.content.Intent;
import android.app.Activity;
import io.codly.Fleesync1.R;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


public class Uploadimg extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_uploadimg);
		initView();
	}

    private void initView(){
        

        //TODO add extra view initialization code here
    }

    
    public void onUploadClicked(View v){
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

    public void onBackClicked(View v){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }


}
