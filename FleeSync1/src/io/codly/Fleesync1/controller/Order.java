package io.codly.Fleesync1.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.app.Activity;
import io.codly.Fleesync1.R;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;


public class Order extends Activity implements Serializable, LocationListener {
	
	Switch onOff;
	Spinner sortdrop;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_order);
		onOff = (Switch)  findViewById(R.id.gpsloc);   
		onOff.setChecked(false);
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		initView();
		
	}
	

    private void initView(){
        
    	//Implementing Spinners for Order form
    	sortdrop = (Spinner)findViewById(R.id.spinnersort);
    	String[] sortitems = new String[]{"Cost","Distance","Time"};
    	
    	ArrayAdapter<String> sortAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sortitems);
    	
    	sortdrop.setAdapter(sortAdapter);
    	
    	Spinner dropdown = (Spinner)findViewById(R.id.spinnerto);
    	String[] items = new String[]{"Akarawita"
    			,"Akuregoda"
    			,"Angoda"
    			,"Athurugiriya"
    			,"Avissawella"
    			,"Batawala"
    			,"Battaramulla"
    			,"Batugampola"
    			,"Bellanwila"
    			,"Bokundara"
    			,"Bope"
    			,"Boralesgamuwa"    			
    			,"Colombo"
    			,"Colombo 1"
    			,"Colombo 10"
    			,"Colombo 11"
    			,"Colombo 12"
    			,"Colombo 13"
    			,"Colombo 14"
    			,"Colombo 15"
    			,"Colombo 2"
    			,"Colombo 3"
    			,"Colombo 4"
    			,"Colombo 5"
    			,"Colombo 7"
    			,"Colombo 8"
    			,"Colombo 9"
    			,"Dehiwala"    			
    			,"Dedigamuwa"
    			,"Deltara"
    			,"Ethul Kotte"
    			,"Gangodawilla"
    			,"Godagama"
    			,"Gonapola"
    			,"Gothatuwa"
    			,"Habarakada"
    			,"Handapangoda"
    			,"Hanwella"
    			,"Hewainna"
    			,"Hiripitya"
    			,"Hokandara"
    			,"Homagama"
    			,"Horagala"
    			,"Ingiriya"
    			,"Jalthara"
    			,"Kaduwela"
    			,"Kahathuduwa"
    			,"Kahawala"
    			,"Kalatuwawa"
    			,"Kaluaggala"
    			,"Kalubowila"
    			,"Katubedda"
    			,"Kelaniya"
    			,"Kesbewa"
    			,"Kiriwattuduwa"
    			,"Kohuwala"
    			,"Kolonnawa"
    			,"Kosgama"
    			,"Koswatta"
    			,"Kotikawatta"
    			,"Kottawa"
    			,"Kotte"
    			,"Madapatha"
    			,"Madiwela"
    			,"Malabe"
    			,"Maradana"
    			,"Mattegoda"
    			,"Meegoda"
    			,"Meepe"
    			,"Mirihana"
    			,"Moragahahena"
    			,"Moraketiya"
    			,"Moratuwa"
    			,"Mount Lavinia"
    			,"Mullegama"
    			,"Mulleriyawa"
    			,"Navagamuwa"
    			,"Nawala"
    			,"Nugegoda"    			
    			,"Padukka"
    			,"Pannipitiya"
    			,"Pelawatta"
    			,"Peliyagoda"
    			,"Pepiliyana"
    			,"Piliyandala"
    			,"Pita Kotte"
    			,"Pitipana Homagama"
    			,"Polgasowita"
    			,"Puwakpitiya"
    			,"Rajagiriya"
    			,"Ranala"
    			,"Ratmalana"
    			,"Siddamulla"
    			,"Sri Jayawardenapura Kotte"
    			,"Talawatugoda"
    			,"Thalapathpitiya"
    			,"Thimbirigasyaya"
    			,"Thummodara"
    			,"Waga"
    			,"Watareka"
    			,"Weliwita"
    			,"Wellampitiya"
    			,"Wellawatte"
    			};
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
    	dropdown.setAdapter(adapter);
    	
    	Spinner dropdown2 = (Spinner)findViewById(R.id.spinnerfrom);
    	String[] items2 = new String[]{ "Akarawita"
    			,"Akuregoda"
    			,"Angoda"
    			,"Athurugiriya"
    			,"Avissawella"
    			,"Batawala"
    			,"Battaramulla"
    			,"Batugampola"
    			,"Bellanwila"
    			,"Bokundara"
    			,"Bope"
    			,"Boralesgamuwa"    			
    			,"Colombo"
    			,"Colombo 1"
    			,"Colombo 10"
    			,"Colombo 11"
    			,"Colombo 12"
    			,"Colombo 13"
    			,"Colombo 14"
    			,"Colombo 15"
    			,"Colombo 2"
    			,"Colombo 3"
    			,"Colombo 4"
    			,"Colombo 5"
    			,"Colombo 7"
    			,"Colombo 8"
    			,"Colombo 9"
    			,"Dehiwala"    			
    			,"Dedigamuwa"
    			,"Deltara"
    			,"Ethul Kotte"
    			,"Gangodawilla"
    			,"Godagama"
    			,"Gonapola"
    			,"Gothatuwa"
    			,"Habarakada"
    			,"Handapangoda"
    			,"Hanwella"
    			,"Hewainna"
    			,"Hiripitya"
    			,"Hokandara"
    			,"Homagama"
    			,"Horagala"
    			,"Ingiriya"
    			,"Jalthara"
    			,"Kaduwela"
    			,"Kahathuduwa"
    			,"Kahawala"
    			,"Kalatuwawa"
    			,"Kaluaggala"
    			,"Kalubowila"
    			,"Katubedda"
    			,"Kelaniya"
    			,"Kesbewa"
    			,"Kiriwattuduwa"
    			,"Kohuwala"
    			,"Kolonnawa"
    			,"Kosgama"
    			,"Koswatta"
    			,"Kotikawatta"
    			,"Kottawa"
    			,"Kotte"
    			,"Madapatha"
    			,"Madiwela"
    			,"Malabe"
    			,"Maradana"
    			,"Mattegoda"
    			,"Meegoda"
    			,"Meepe"
    			,"Mirihana"
    			,"Moragahahena"
    			,"Moraketiya"
    			,"Moratuwa"
    			,"Mount Lavinia"
    			,"Mullegama"
    			,"Mulleriyawa"
    			,"Navagamuwa"
    			,"Nawala"
    			,"Nugegoda"    			
    			,"Padukka"
    			,"Pannipitiya"
    			,"Pelawatta"
    			,"Peliyagoda"
    			,"Pepiliyana"
    			,"Piliyandala"
    			,"Pita Kotte"
    			,"Pitipana Homagama"
    			,"Polgasowita"
    			,"Puwakpitiya"
    			,"Rajagiriya"
    			,"Ranala"
    			,"Ratmalana"
    			,"Siddamulla"
    			,"Sri Jayawardenapura Kotte"
    			,"Talawatugoda"
    			,"Thalapathpitiya"
    			,"Thimbirigasyaya"
    			,"Thummodara"
    			,"Waga"
    			,"Watareka"
    			,"Weliwita"
    			,"Wellampitiya"
    			,"Wellawatte"
    			};
    	ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items2);
    	dropdown2.setAdapter(adapter2);
    }
    
   
    
    
    public void onBackClicked(View v){
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

    public void onConfirmClicked(View v){
    	
    	String sortby=sortdrop.getSelectedItem().toString();
    	
    	//Check whether the get current location switch is on/off
    	 	   	
    	onOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
    	    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
    	    	
    	        //Disabling From Spinner
    	    	Spinner spinner = (Spinner) findViewById(R.id.spinnerfrom);
    	    	spinner.setEnabled(false);
    	    	
    			
    	    }
    	    
    	});
    	String pNo = null;
    	List<NameValuePair> params;
    	params = new ArrayList<NameValuePair>();
    	
    	Bundle extras = getIntent().getExtras();    	    	    
    	pNo=extras.getString("pno");
    	
    	
    	if(onOff.isChecked()){
    		
    		//If get location by GPS is off
        	//Get Destination        	
    		Spinner to = (Spinner)findViewById(R.id.spinnerto);
    		String destination = to.getSelectedItem().toString();
    		//Get Location        	
    		Spinner from = (Spinner)findViewById(R.id.spinnerfrom);
    		String Location = from.getSelectedItem().toString();
    		
    		String destinationlat="6.9344";
    		String destinationlong="79.8428";
    		
    		String locationlat="6.8219";
    		String locationlong="79.8861";
    		
    		params.add(new BasicNameValuePair("pno", pNo));
    		params.add(new BasicNameValuePair("DestinationLat", destinationlat));
            params.add(new BasicNameValuePair("DestinationLong", destinationlong));
            params.add(new BasicNameValuePair("LocationLat", locationlat));
            params.add(new BasicNameValuePair("LocationLong", locationlong));
            
            ServerRequest sr = new ServerRequest();
            //JSONObject json = sr.getJSON("http://10.0.3.2:8080/order",params);
            
            JSONObject json = sr.getJSON("http://fleetride-ridesharefleet.rhcloud.com/route",params);
            
            
            
            if(json != null){
            try{
            String jsonstr = json.getString("response");
                if(json.getBoolean("res")){
                	Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
                }
                else{
               	 Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
                }
                

            }catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "Invalid response", Toast.LENGTH_SHORT).show();
            }
            }
    		
    		Toast.makeText(this, "Order completed select best taxi for you", Toast.LENGTH_SHORT).show();
        	
        	     	
        	Intent intent = new Intent(getBaseContext(), Taxilist.class);
        	intent.putExtra("pNo", pNo); 
        	intent.putExtra("DestinationLat", destinationlat);     
        	intent.putExtra("DestinationLon", destinationlong); 
        	intent.putExtra("LocationLat", locationlat); 
        	intent.putExtra("LocationLot", locationlong); 
        	intent.putExtra("Sortby", sortby);
            startActivity(intent);
    		
    	}
    	else if(!onOff.isChecked()){
    		
    		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		    Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		    double latitude=0;
		    double longitude=0;
		    String locationlat="6.9344";
		    String locationlong="79.8428";
		    String destinationlat="6.9344";
    		String destinationlong="79.8428";
		    latitude = location.getLatitude();
		    longitude = location.getLongitude();
    		
    		locationlat=String.valueOf(location.getLatitude());
    		locationlong=String.valueOf(location.getLongitude());
    		
    		params.add(new BasicNameValuePair("pno", pNo));
    		params.add(new BasicNameValuePair("DestinationLat", destinationlat));
            params.add(new BasicNameValuePair("DestinationLong", destinationlong));
            params.add(new BasicNameValuePair("LocationLat", locationlat));
            params.add(new BasicNameValuePair("LocationLong", locationlong));
            
            ServerRequest sr = new ServerRequest();
            //JSONObject json = sr.getJSON("http://10.0.3.2:8080/order",params);
            
            JSONObject json = sr.getJSON("http://fleetride-ridesharefleet.rhcloud.com/route",params);
            
            
            
            if(json != null){
            try{
            String jsonstr = json.getString("response");
                if(json.getBoolean("res")){
                	Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
                }
                else{
               	 Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
                }
                

            }catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "Invalid response", Toast.LENGTH_SHORT).show();
            }
            }
    		
    		Toast.makeText(this, "Order completed select best taxi for you", Toast.LENGTH_SHORT).show();
        	
        	     	
        	Intent intent = new Intent(getBaseContext(), Taxilist.class);
        	intent.putExtra("pNo", pNo); 
        	intent.putExtra("DestinationLat", destinationlat);     
        	intent.putExtra("DestinationLon", destinationlong); 
        	intent.putExtra("LocationLat", locationlat); 
        	intent.putExtra("LocationLot", locationlong); 
        	intent.putExtra("Sortby", sortby);
            startActivity(intent);
			
    	}
    	
    	
    }


	@Override
	public void onLocationChanged(Location location) {
		 
		
	}


	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
    


}
