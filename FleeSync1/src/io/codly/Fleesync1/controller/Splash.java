package io.codly.Fleesync1.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import io.codly.Fleesync1.R;

public class Splash extends Activity {

    @
    Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_splash);
        new Handler().postDelayed(new Runnable() {

            // Using handler with postDelayed called runnable run method
            @
            Override
            public void run() {
                Intent i = new Intent(Splash.this, Login.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, 3000); // wait for 3 seconds

    }

}
