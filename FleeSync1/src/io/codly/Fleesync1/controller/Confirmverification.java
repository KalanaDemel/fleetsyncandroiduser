package io.codly.Fleesync1.controller;

import android.os.Bundle;
import android.content.Intent;
import android.app.Activity;
import io.codly.Fleesync1.R;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


public class Confirmverification extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_confirmverification);
		initView();
	}

    private void initView(){
        

        //TODO add extra view initialization code here
    }

    
    public void onVerifyClicked(View v){
    	
    	//Getting the user inserted code from textbox
    	EditText codeText = (EditText)findViewById(R.id.phoneno);		
		String code = codeText.getText().toString();
		
		//Retrieving code from previous intent
    	Bundle extras = getIntent().getExtras();
    	if (extras != null) {
    	    String conCode = extras.getString("code");
    	    String pNo=extras.getString("pno");
    	    
    	    //Verification of code
    	    if(code.equalsIgnoreCase(conCode)){    	    	
    	    	Toast.makeText(this, "You entered the valid code", Toast.LENGTH_SHORT).show();   
    	    	Intent intent = new Intent(this, Register.class);
    	    	intent.putExtra("pno", pNo);
    	        startActivity(intent);
    	    }
    	    else{
    	    	Toast.makeText(this, "Code Invalid please recheck code", Toast.LENGTH_SHORT).show();
    	    }
    	}
    	        
    }

    public void onBackClicked(View v){
        Intent intent = new Intent(this, Confirmation.class);
        startActivity(intent);
    }


}
