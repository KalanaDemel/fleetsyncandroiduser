package io.codly.Fleesync1.controller;

import android.R.bool;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.app.Activity;
import android.app.Dialog;
import io.codly.Fleesync1.R;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Login extends Activity {
	
	
	 
	 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_login);
		
		
        initView();
        
        ImageView img = (ImageView)findViewById(R.id.imageView1);
        img.setBackgroundResource(R.drawable.myprogress);

        // Get the background, which has been compiled to an AnimationDrawable object.
        AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();

        // Start the animation (looped playback by default).
        frameAnimation.start();
	}

    private void initView(){
        

        //TODO add extra view initialization code here
    }

    
    public void onSigninClicked(View v){
    	
    	//Intent intent = new Intent(this, Home.class);
   	 	//intent.putExtra("pno", "0755240740");
        //startActivity(intent);
    	
    	EditText pno, password ; 
   	 	List<NameValuePair> params;
   	 	
   	 	
		pno = (EditText)findViewById(R.id.user);
        password = (EditText)findViewById(R.id.password);
    	
    	
    	 String phone = pno.getText().toString();
         String passwordtxt = password.getText().toString();
         params = new ArrayList<NameValuePair>();
         params.add(new BasicNameValuePair("pno", phone));
         params.add(new BasicNameValuePair("password", passwordtxt));
         
         
         
       
        
        	 ServerRequest sr = new ServerRequest();
             JSONObject json = sr.getJSON("http://nodejs-fleetsyncanrd.rhcloud.com/login",params);
             
             if(json != null){
             try{
             String jsonstr = json.getString("response");
                 if(json.getBoolean("res")){
                     
                	 Intent intent = new Intent(this, Home.class);
                	 intent.putExtra("pno", phone);
                     startActivity(intent);
                 }
                 else{
                	 Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();
                 }
                 Toast.makeText(getApplication(),jsonstr,Toast.LENGTH_LONG).show();

             }catch (JSONException e) {
                 e.printStackTrace();
                 Toast.makeText(this, "Error Invalid login", Toast.LENGTH_SHORT).show();
             }
             }
         
         
         
              
    }

    public void onRegisterClicked(View v){
        Intent intent = new Intent(this, Confirmation.class);
        startActivity(intent);
    }

    public void onRecoverClicked(View v){
        Intent intent = new Intent(this, Resetpw.class);
        startActivity(intent);
    }
    
    
    
    
    private boolean isNetworkConnected() {
   	  ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
   	  NetworkInfo ni = cm.getActiveNetworkInfo();
   	  if (ni == null) {
   	   // There are no active networks.
   	   return false;
   	  } else
   	   return true;
   	 }


}
